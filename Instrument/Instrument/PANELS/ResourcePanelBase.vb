Imports System.ComponentModel
Imports isr.Core.Pith
''' <summary> Provides a base user interface for a <see cref="isr.VI.DeviceBase">Visa Device</see>. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="3.0.4955"> created based on the legacy VISA
''' resource panel. </history>
<System.ComponentModel.Description("Resource Panel Base Control")>
<System.Drawing.ToolboxBitmap(GetType(ResourcePanelBase), "Panels.ResourcePanelBase.bmp"), ToolboxItem(True)>
Public Class ResourcePanelBase
    Inherits TalkerControlBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Public Sub New()
        Me.New(Nothing)
    End Sub

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="device"> The connectable resource. </param>
    Protected Sub New(ByVal device As DeviceBase)
        MyBase.New()
        Me.InitializeComponent()
        Me._ElapsedTimeStopwatch = New Stopwatch
        Me._AssignDevice(device)
        Me.Connector.Connectible = True
        Me.Connector.Clearable = True
        Me.StatusRegisterLabel.Visible = False
        Me.StandardRegisterLabel.Visible = False
        Me.IdentityLabel.Visible = False
        Me._StatusRegisterStatus = -1
        Me._StandardRegisterStatus = -1
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.Device IsNot Nothing Then
                    Try
                        ' this is required to release the device event handlers associated with this panel. 
                        If Me.Device IsNot Nothing Then Me.DeviceClosing(Me, New System.ComponentModel.CancelEventArgs)
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, "Exception occurred closing the device", "Exception details: {0}", ex)
                    End Try
                    Try
                        ' this also releases the device event handlers associated with this panel. 
                        Me.ReleaseDevice()
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, "Exception occurred releasing the device", "Exception details: {0}", ex)
                    End Try
                    Try
                        If Me.IsDeviceOwner Then
                            ' this also closes the session. 
                            Me._Device.Dispose()
                        End If
                        Me._Device = Nothing
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing the device", "Exception details: {0}", ex)
                    End Try
                End If
                Me._ElapsedTimeStopwatch = Nothing
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing

            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 1/4/2016. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me.StatusLabel.Text = "Find and select a resource."
            Me.IdentityLabel.Text = ""
            Me.StatusRegisterLabel.Visible = True
            Me.StandardRegisterLabel.Visible = False
            Me.IdentityLabel.Visible = False
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " SAFE SETTERS "

    ''' <summary> Recursively enable. </summary>
    ''' <remarks> David, 1/6/2016. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   True to show or False to hide the control. </param>
    Protected Sub RecursivelyEnable(ByVal control As System.Windows.Forms.Control, ByVal value As Boolean)
        If control IsNot Nothing Then
            control.Enabled = value
            If control.Controls IsNot Nothing AndAlso control.Controls.Count > 0 Then
                For Each c As System.Windows.Forms.Control In control.Controls
                    Me.RecursivelyEnable(c, value)
                Next
            End If
        End If
    End Sub

    ''' <summary> Safe visible setter. </summary>
    ''' <remarks> David, 1/6/2016. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   True to show or False to hide the control. </param>
    Private Shared Sub SafeVisibleSetter(ByVal control As System.Windows.Forms.ToolStripItem, value As Boolean)
        If control IsNot Nothing AndAlso control.Owner IsNot Nothing AndAlso control.Visible <> value Then
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of System.Windows.Forms.ToolStripItem, Boolean)(AddressOf ResourcePanelBase.SafeVisibleSetter), New Object() {control, value})
            Else
                control.Visible = value
            End If
        End If
    End Sub

    ''' <summary> Safe text setter. </summary>
    ''' <remarks> David, 1/6/2016. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   True to show or False to hide the control. </param>
    Private Shared Sub SafeTextSetter(ByVal control As System.Windows.Forms.ToolStripItem, value As String)
        If control IsNot Nothing AndAlso control.Owner IsNot Nothing AndAlso
            Not String.Equals(control.Text, value) Then
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of System.Windows.Forms.ToolStripItem, String)(AddressOf ResourcePanelBase.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
        End If
    End Sub

    ''' <summary> Safe tool tip text setter. </summary>
    ''' <remarks> David, 1/6/2016. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   True to show or False to hide the control. </param>
    Private Shared Sub SafeToolTipTextSetter(ByVal control As System.Windows.Forms.ToolStripItem, value As String)
        If control IsNot Nothing AndAlso control.Owner IsNot Nothing AndAlso
            Not String.Equals(control.Text, value) Then
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of System.Windows.Forms.ToolStripItem, String)(AddressOf ResourcePanelBase.SafeTextSetter), New Object() {control, value})
            Else
                control.ToolTipText = value
            End If
        End If
    End Sub

#End Region

#Region " REGISTER DISPLAY SETTERS "


    ''' <summary> Shows or hides the identity display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub IdentityVisibleSetter(ByVal value As Boolean)
        ResourcePanelBase.SafeVisibleSetter(Me.IdentityLabel, value)
    End Sub

    ''' <summary> Show or hide the status register display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub StatusRegisterVisibleSetter(ByVal value As Boolean)
        ResourcePanelBase.SafeVisibleSetter(Me.StatusRegisterLabel, value)
    End Sub

    ''' <summary> Displays the status register status using hex format. </summary>
    ''' <param name="value"> The register value. </param>
    Public Overridable Sub DisplayStatusRegisterStatus(ByVal value As Integer)
        Me.DisplayStatusRegisterStatus(value, "0x{0:X2}")
    End Sub

    Private _StatusRegisterStatus As Integer
    ''' <summary> Displays the status register status. </summary>
    ''' <param name="value">  The register value. </param>
    ''' <param name="format"> The format. </param>
    Public Sub DisplayStatusRegisterStatus(ByVal value As Integer, ByVal format As String)
        If Not value.Equals(Me._StatusRegisterStatus) Then
            Me._StatusRegisterStatus = value
            ResourcePanelBase.SafeTextSetter(Me.StatusRegisterLabel, String.Format(format, value))
        End If
    End Sub

    ''' <summary> Shows or hides the Standard Register display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub StandardRegisterVisibleSetter(ByVal value As Boolean)
        If Me.StandardRegisterLabel.Visible <> value Then ResourcePanelBase.SafeVisibleSetter(Me.StandardRegisterLabel, value)
    End Sub

    ''' <summary> Displays the standard register status. Uses Hex format. </summary>
    ''' <remarks> David, 1/2/2016. </remarks>
    ''' <param name="value"> The register value. </param>
    Public Sub DisplayStandardRegisterStatus(ByVal value As StandardEvents?)
        If value.HasValue Then Me.DisplayStatusRegisterStatus(CInt(value.Value))
    End Sub

    ''' <summary> Displays the standard register status. Uses Hex format. </summary>
    ''' <param name="value"> The register value. </param>
    Public Sub DisplayStandardRegisterStatus(ByVal value As Integer)
        Me.DisplayStandardRegisterStatus(value, "0x{0:X2}")
    End Sub

    Private _StandardRegisterStatus As Integer
    ''' <summary> Displays the standard register status. </summary>
    ''' <param name="value">  The register value. </param>
    ''' <param name="format"> The format. </param>
    Public Sub DisplayStandardRegisterStatus(ByVal value As Integer?, ByVal format As String)
        Me.StandardRegisterVisibleSetter(value.HasValue)
        If value.HasValue AndAlso Not value.Value.Equals(Me._StandardRegisterStatus) Then
            Me._StandardRegisterStatus = value.Value
            ResourcePanelBase.SafeTextSetter(Me.StatusRegisterLabel, String.Format(format, value.Value))
        End If
    End Sub

#End Region

#Region " RESOURCE NAME "

    Private _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ResourceName) Then
                Me._ResourceName = value
                ResourcePanelBase.SafeTextSetter(Me.IdentityLabel, Me.ResourceName)
                ResourcePanelBase.SafeToolTipTextSetter(Me.IdentityLabel, Me.ResourceName)
                Me.AsyncNotifyPropertyChanged(NameOf(Me.ResourceName))
            End If
            Me.Connector.SelectedResourceName = value
        End Set
    End Property

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    <Category("Appearance"), Description("The format of the open resource title"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("{0}.{1}")>
    Public Property OpenResourceTitleFormat As String

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    <Category("Appearance"), Description("The format of the close resource title"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("{0}")>
    Public Property ClosedResourceTitleFormat As String

    Private _ResourceTitle As String
    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    <Category("Appearance"), Description("The resource title"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("INSTR")>
    Public Property ResourceTitle As String
        Get
            Return Me._ResourceTitle
        End Get
        Set(value As String)
            Me._ResourceTitle = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Builds the title. </summary>
    ''' <remarks> David, 1/4/2016. </remarks>
    ''' <returns> A String. </returns>
    Protected Function BuildTitle() As String
        If Me.IsDeviceOpen Then
            If String.IsNullOrWhiteSpace(Me.OpenResourceTitleFormat) OrElse String.IsNullOrWhiteSpace(Me.ResourceTitle) Then
                Return ""
            Else
                Return String.Format(Me.OpenResourceTitleFormat, Me.ResourceTitle, Me.ResourceName)
            End If
        Else
            If String.IsNullOrWhiteSpace(Me.ClosedResourceTitleFormat) OrElse String.IsNullOrWhiteSpace(Me.ResourceTitle) Then
                Return ""
            Else
                Return String.Format(Me.ClosedResourceTitleFormat, Me.ResourceTitle)
            End If
        End If
    End Function

#End Region

#Region " DEVICE And CONNECTOR "

    ''' <summary> Gets or sets the elapsed time stop watch. </summary>
    ''' <value> The elapsed time stop watch. </value>
    Protected ReadOnly Property ElapsedTimeStopwatch As Stopwatch

    ''' <summary> Gets or sets the sentinel indicating if this panel owns the device and, therefore, needs to 
    '''           dispose of this device. </summary>
    ''' <value> The is device owner. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property IsDeviceOwner As Boolean

    ''' <summary> Releases the device. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    Private Sub _ReleaseDevice()
        If Me.Device IsNot Nothing Then
            Me.Device.Talker.Listeners.Clear()
            RemoveHandler Me.Device.PropertyChanged, AddressOf Me.DevicePropertyChanged
            RemoveHandler Me.Device.Opening, AddressOf Me.DeviceOpening
            RemoveHandler Me.Device.Opened, AddressOf Me.DeviceOpened
            RemoveHandler Me.Device.Closing, AddressOf Me.DeviceClosing
            RemoveHandler Me.Device.Closed, AddressOf Me.DeviceClosed
            RemoveHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
            RemoveHandler Me.Device.Initializing, AddressOf Me.DeviceInitializing
            Me.Connector.ResourcesFilter = ""
            ' note that service request is released when device closes.
        End If
    End Sub

    ''' <summary> Releases the device. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    Protected Overridable Sub ReleaseDevice()
        Me._ReleaseDevice()
    End Sub

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub _AssignDevice(ByVal value As VI.DeviceBase)
        Me._ReleaseDevice()
        Me._Device = value
        If value IsNot Nothing Then
            'Me._Device.AddListeners(Me.talker.listeners)
            AddHandler Me.Device.PropertyChanged, AddressOf Me.DevicePropertyChanged
            AddHandler Me.Device.Opening, AddressOf Me.DeviceOpening
            AddHandler Me.Device.Opened, AddressOf Me.DeviceOpened
            AddHandler Me.Device.Closing, AddressOf Me.DeviceClosing
            AddHandler Me.Device.Closed, AddressOf Me.DeviceClosed
            AddHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
            AddHandler Me.Device.Initializing, AddressOf Me.DeviceInitializing
            If Me.Device.ResourcesFilter IsNot Nothing Then
                Me.Connector.ResourcesFilter = Me.Device.ResourcesFilter
            End If
        End If
    End Sub

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overridable Sub AssignDevice(ByVal value As VI.DeviceBase)
        Me._AssignDevice(value)
    End Sub

    ''' <summary> Gets or sets reference to the VISA <see cref="VI.DeviceBase">device</see>
    ''' interfaces. </summary>
    ''' <value> The connectable resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property Device() As VI.DeviceBase

    ''' <summary> Gets the sentinel indicating if the device is open. </summary>
    ''' <value> The is device open. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me.Device IsNot Nothing AndAlso Me.Device.IsDeviceOpen
        End Get
    End Property

#Region " SERVICE REQUESTED "

    ''' <summary> Event handler. Called when the device is requesting service. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Message based session event information. </param>
    Protected Overridable Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Private _IsServiceRequestEventEnabled As Boolean
    Private _IsDeviceServiceRequestEnabled As Boolean

    ''' <summary> Enables service request event. </summary>
    ''' <remarks>
    ''' The panel can enable the service request if owner or if not owner but service request was not
    ''' enabled on the device, in which case, the panel could also disable the service request.
    ''' </remarks>
    Public Overridable Sub EnableServiceRequestEventHandler()
        If Not Me._IsServiceRequestEventEnabled Then
            AddHandler Me.Device.ServiceRequested, AddressOf Me.DeviceServiceRequested
            If Me.IsDeviceOwner Then
                Me.Device.EnableServiceRequestEventHandler()
            ElseIf Not Me.Device.Session.IsServiceRequestEventEnabled Then
                Me.Device.EnableServiceRequestEventHandler()
                Me._IsDeviceServiceRequestEnabled = True
            End If
        End If
    End Sub

    ''' <summary> Disables service request event. </summary>
    Public Overridable Sub DisableServiceRequestEventHandler()
        If Me._IsServiceRequestEventEnabled Then
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.DeviceServiceRequested
            If Me.IsDeviceOwner OrElse Me._IsDeviceServiceRequestEnabled Then Me.Device.DisableServiceRequestEventHandler()
            Me._IsServiceRequestEventEnabled = False
            Me._IsDeviceServiceRequestEnabled = False
        End If
    End Sub

#End Region

#Region " PROPERTY CHANGE "

    ''' <summary> Executes the subsystem property changed action. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Sub OnPropertyChanged(ByVal subsystem As StatusSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse propertyName Is Nothing Then Return
        Select Case propertyName
            Case NameOf(subsystem.ErrorAvailable)
                If Not subsystem.ReadingDeviceErrors AndAlso subsystem.ErrorAvailable Then
                    Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error available;. ")
                End If
            Case NameOf(subsystem.MessageAvailable)
                If subsystem.MessageAvailable Then
                    Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Message available;. ")
                End If
            Case NameOf(subsystem.MeasurementAvailable)
                If subsystem.MeasurementAvailable Then
                    Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Measurement available;. ")
                End If
            Case NameOf(subsystem.ReadingDeviceErrors)
                If subsystem.ReadingDeviceErrors Then
                    Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Reading device errors;. ")
                End If
            Case NameOf(subsystem.ServiceRequestStatus)
                Me.DisplayStatusRegisterStatus(subsystem.ServiceRequestStatus)
            Case NameOf(subsystem.StandardEventStatus)
                Me.DisplayStandardRegisterStatus(subsystem.StandardEventStatus)
        End Select
    End Sub

    ''' <summary> Handle the device property changed event. </summary>
    ''' <param name="device">    The device. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnDevicePropertyChanged(ByVal device As DeviceBase, ByVal propertyName As String)
        If device Is Nothing OrElse propertyName Is Nothing Then Return
        Select Case propertyName
            Case NameOf(device.Enabled)
                Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   $"{device.ResourceTitle} {device.Enabled.GetHashCode:enabled;enabled;disabled};. ")
            Case NameOf(device.ResourcesFilter)
                Me.Connector.ResourcesFilter = device.ResourcesFilter
            Case NameOf(device.ServiceRequestFailureMessage)
                If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                    Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                End If
            Case NameOf(device.ResourceTitle)
                Me.ResourceTitle = device.ResourceTitle
                Me.OnTitleChanged(Me.BuildTitle)
            Case NameOf(device.ResourceName)
                Me.ResourceName = device.ResourceName
                Me.OnTitleChanged(Me.BuildTitle)
        End Select
    End Sub

    ''' <summary> Event handler. Called for property changed events. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    ''' <see cref="Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                Me.OnDevicePropertyChanged(TryCast(sender, DeviceBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception handling property '{0}' changed event;. Details: {1}", e.PropertyName, ex)
        End Try
    End Sub

#End Region

#Region " OPENING / OPEN "

    ''' <summary> Connects the instrument by opening a visa session. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:   DoNotCatchGeneralExceptionTypes")>
    Private Sub openSession(ByVal resourceName As String, ByVal resourceTitle As String)
        Try
            Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Opening {0};. ", resourceName)
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            If Me.Device.TryOpenSession(resourceName, resourceTitle) Then
                Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Opened {0};. ", resourceName)
            Else
                Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Open {0} Failed;. ", resourceName)
            End If
        Catch ex As OperationFailedException
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed opening {0};. Details: {1}", resourceName, ex)
        Catch ex As Exception
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception opening {0};. Details: {1}", resourceName, ex)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Connects the instrument by calling a propagating connect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_Connect(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Connector.Connect
        Me.openSession(Me.Connector.SelectedResourceName, Me.ResourceTitle)
        ' cancel if failed to open
        If Not Me.IsDeviceOpen Then e.Cancel = True
    End Sub

    ''' <summary> Event handler. Called upon device opening. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceOpening(ByVal sender As Object, ByVal e As ComponentModel.CancelEventArgs)
        Me.StatusRegisterLabel.Text = "0x.."
        Me.StandardRegisterLabel.Text = "0x.."
    End Sub

    ''' <summary> Event handler. Called when device opened. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ResourceName = Me.Device.ResourceName
        If Not String.IsNullOrEmpty(Me.ResourceTitle) Then Me.Device.Session.ResourceTitle = Me.ResourceTitle
        Dim outcome As TraceEventType = TraceEventType.Information
        If Me.Device.Session.Enabled And Not Me.Device.Session.IsSessionOpen Then outcome = TraceEventType.Warning
        Me.Talker?.Publish(outcome, My.MyLibrary.TraceEventId,
                           "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                           Me.ResourceTitle,
                           Me.Device?.Session.Enabled.GetHashCode,
                           Me.Device?.Session.IsDeviceOpen.GetHashCode,
                           Me.Device?.Session.IsSessionOpen.GetHashCode)
    End Sub

#End Region

#Region " INITALIZING / INITIALIZED  "

    Protected Overridable Sub DeviceInitializing(ByVal sender As Object, ByVal e As ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    ''' <summary> Executes the title changed action. </summary>
    ''' <remarks> David, 1/14/2016. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Protected Overridable Sub OnTitleChanged(ByVal value As String)
        Me.Title = value
    End Sub

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Title As String

#End Region

#Region " CLOSING / CLOSED "

    ''' <summary> Disconnects the instrument by closing the open session. </summary>
    Private Sub closeSession()
        If Me.Device.IsDeviceOpen Then
            Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Releasing {0};. ", Me.Device.ResourceName)
        End If
        If Me.Device.TryCloseSession() Then
            Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Released {0};. ", Me.Connector.SelectedResourceName)
        Else
            Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Failed releasing {0};. ", Me.Connector.SelectedResourceName)
        End If
    End Sub

    ''' <summary> Disconnects the instrument by calling a propagating disconnect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_Disconnect(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Connector.Disconnect
        Me.closeSession()
        ' Cancel if failed to closed
        If Me.IsDeviceOpen Then e.Cancel = True
    End Sub

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As ComponentModel.CancelEventArgs)
        Me.DisableServiceRequestEventHandler()
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.StatusRegisterLabel.Visible = False
        Me.StandardRegisterLabel.Visible = False
        Me.IdentityLabel.Visible = False
        Me.StandardRegisterVisibleSetter(False)
        Me.StatusRegisterVisibleSetter(False)
        If Me.Device IsNot Nothing Then
            Me.DisableServiceRequestEventHandler()
            If Me.Device.Session.IsSessionOpen Then
                Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Device closed but session still open;. ")
            ElseIf Me.Device.Session.IsDeviceOpen Then
                Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Device closed but emulated session still open;. ")
            Else
                Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnected;. Device access closed.")
            End If
        Else
            Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Disconnected;. Device disposed.")
        End If
    End Sub

#End Region

#End Region

#Region " CONNECTOR SELECTOR EVENT HANDLERS "

    ''' <summary> Displays the resource names based on the device resource search pattern. </summary>
    Public Sub DisplayNames()
        ' get the list of available resources
        If Me.Device IsNot Nothing AndAlso Me.Device.ResourcesFilter IsNot Nothing Then
            Me.Connector.ResourcesFilter = Me.Device.ResourcesFilter
        End If
        Me.Connector.DisplayResourceNames()
    End Sub

    ''' <summary> Clears the instrument by calling a propagating clear command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Clear
        Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                           "Clearing resource;. Clearing the resource {0}", Me.Connector.SelectedResourceName)
        Me.Device.ResetAndClear()
        Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                           "Resource cleared;. Resource {0} cleared", Me.Connector.SelectedResourceName)
    End Sub

    ''' <summary> Displays available instrument names. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary> Executes the property changed action. </summary>
    ''' <remarks> David, 1/13/2016. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As ResourceSelectorConnector, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(sender.SelectedResourceName)
                Me.ResourceName = sender.SelectedResourceName
                Me.Talker?.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Selected {0};. ", sender.SelectedResourceName)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _ResourceNameSelectorConnector for property changed
    ''' events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub connector_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles Connector.PropertyChanged
        Try
            Me.OnPropertyChanged(TryCast(sender, ResourceSelectorConnector), e?.PropertyName)
        Catch ex As Exception
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception handling property '{0}' changed Event;. Details: {1}", e.PropertyName, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Adds the listeners such as the current trace messages box. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    Protected Overridable Overloads Sub AddListeners()
        Me.Talker.Listeners.Add(Me.TraceMessagesBox)
        Me.Connector.AddListeners(Me.Talker.Listeners)
        Me.Device.AddListeners(Me.Talker.Listeners)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="listeners"> The listeners. </param>
    Public Overrides Sub AddListeners(ByVal listeners As IEnumerable(Of ITraceMessageListener))
        MyBase.AddListeners(listeners)
        Me.Connector.AddListeners(listeners)
        Me.Device.AddListeners(listeners)
    End Sub

    ''' <summary> Adds the log listener. </summary>
    ''' <remarks> David, 1/21/2016. </remarks>
    ''' <param name="log"> The log. </param>
    Public Overridable Overloads Sub AddListeners(ByVal log As MyLog)
        Me.AddListeners(New ITraceMessageListener() {log})
    End Sub

    ''' <summary> Executes the trace messages box property changed action. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnTraceMessagesBoxPropertyChanged(sender As TraceMessagesBox, ByVal propertyName As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            If String.Equals(propertyName, NameOf(sender.StatusPrompt)) Then
                Me._StatusLabel.Text = sender.StatusPrompt
                Me._StatusLabel.ToolTipText = sender.StatusPrompt
            End If
        End If
    End Sub

    ''' <summary> Trace messages box property changed. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _TraceMessagesBox_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles TraceMessagesBox.PropertyChanged
        Try
            Me.OnTraceMessagesBoxPropertyChanged(TryCast(sender, TraceMessagesBox), e?.PropertyName)
        Catch ex As Exception
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Failed reporting Trace Message Property Change;. Details: {0}", ex)
        End Try

    End Sub

#End Region

End Class

